
import java.util.Scanner;

class TwoSum {
    public int[] twoSum(int[] nums, int target) {
        int[] result = new int[2];
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    result[0] = i;
                    result[1] = j;
                    return result;
                }
            }
        }
        return result; // If no such pair is found, return [0, 0].
    }

    public static void main(String[] args) {
        TwoSum twoSum = new TwoSum();
        Scanner scanner = new Scanner(System.in);

        // Input the number of elements in the array
        System.out.print("Enter the number of elements in the array: ");
        int numElements = scanner.nextInt();
        int[] nums = new int[numElements];

        // Input the array elements
        System.out.print("Enter the array elements (space-separated): ");
        for (int i = 0; i < numElements; i++) {
            nums[i] = scanner.nextInt();
        }

        // Input the target
        System.out.print("Enter the target sum: ");
        int target = scanner.nextInt();

        int[] result = twoSum.twoSum(nums, target);

        if (result[0] != 0 || result[1] != 0) {
            System.out.println("Indices of the two numbers that add up to the target: [" + result[0] + ", " + result[1] + "]");
        } else {
            System.out.println("No two numbers add up to the target.");
        }
    }
}

