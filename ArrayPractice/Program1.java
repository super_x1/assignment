//sum of max and min in the array
import java.util.*;
class Program1 {
      public static void main(String[] args) {
         Scanner sc = new Scanner(System.in);
	  System.out.println("Enter array size ");
	  int n = sc.nextInt();
	  int arr[] = new int[n];
	 System.out.println("Enter elements ");
	  for(int k = 0;k <= n - 1;k++) {
	      arr[k] = sc.nextInt();
	  }
	  int sum = 0;
	  int min = Integer.MAX_VALUE;
          int max = Integer.MIN_VALUE;
          
	   for (int num : arr) {
            if (num < min) {
                min = num;
            }
            if (num > max) {
                max = num;
            }
          }
         sum = min + max;
	System.out.println("The sum of max and min is " + sum);
       }
}
