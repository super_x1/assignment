//Product array puzzle
import java.util.Scanner;

class Program6 { 
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of elements in the array: ");
        int n = sc.nextInt();
        int[] arr = new int[n];
        int[] prodArr = new int[n];
        int prod = 1;
        for (int i = 0; i < n; i++) {
            System.out.print("Enter element " + (i + 1) + ": ");
            arr[i] = sc.nextInt();
            prod *= arr[i];
        }
        for (int i = 0; i < n; i++) {
            prodArr[i] = prod / arr[i];
        }
        System.out.print("The product array is: ");
        for (int i = 0; i < n; i++) {
            System.out.print(prodArr[i] + " ");
        }
    }
}
