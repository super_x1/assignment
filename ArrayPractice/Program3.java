//Range sum query

import java.util.Scanner;

class Program3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
	System.out.println("Enter the  first array size:");
        int n = sc.nextInt();
        int[] A = new int[n];
	System.out.println("Enter the  first array elements: ");
        for (int i = 0; i < n; i++) {
            A[i] = sc.nextInt();
        }
	System.out.println("Enter the  second array size:");
        int m = sc.nextInt();
        int[][] B = new int[m][2];
	System.out.println("Enter the  second array elements: ");
        for (int i = 0; i < m; i++) {
            B[i][0] = sc.nextInt();
            B[i][1] = sc.nextInt();
        }
        for (int i = 0; i <= m - 1; i++) {
            int L = B[i][0];
            int R = B[i][1];
            int sum = 0;
            for (int j = L - 1; j < R; j++) {
                sum += A[j];
            }
            System.out.println("Sum of elements in range [" + L + ", " + R + "]: " + sum);
        }
    }
}

