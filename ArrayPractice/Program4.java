//time to equality
import java.util.Scanner;
class Program4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
	System.out.print("Enter size of  the array: ");
        int n = sc.nextInt();
        int[] arr = new int[n];
	System.out.print("Enter the number of elements in the array: ");
	

        int min = Integer.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
            if (arr[i] < min) {
                min = arr[i];
            }
        }
        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum += arr[i] - min;
        }
        System.out.println("Minimum time required to make all elements of the array equal: " + sum + " seconds");
    }
}
