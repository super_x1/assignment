//leaders in array
import java.util.*; 
class Program7 {
  public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of elements in the array: ");
        int n = sc.nextInt();
        int[] arr = new int[n];
        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
            findLeaders(arr);
    }

    public static void findLeaders(int[] arr) {
        int n = arr.length;
        int maxRight = arr[n - 1];
        System.out.println("Leaders in the array:");
        System.out.print(maxRight + " ");
        for (int i = n - 2; i >= 0; i--) {
            if (arr[i] > maxRight){
                System.out.print(arr[i] + " ");
                maxRight = arr[i];
            }
	}
    }
}
