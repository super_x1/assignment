//In-prefix sum
import java.util.Scanner;

class Program5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the size of the array: ");
        int N = sc.nextInt();
        int[] A = new int[N];
        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < N; i++) {
            A[i] = sc.nextInt();
        }
        for (int i = 1; i < N; i++) {
            A[i] += A[i - 1];
        }
        System.out.println("The prefix sum of the array is:");
        for (int i = 0; i < N; i++) {
            System.out.print(A[i] + " ");
        }
    }
}
