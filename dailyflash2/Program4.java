//print composite numbers in the given numers

import java.util.Scanner;

class CompositeNum {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the  start: ");
        int start = input.nextInt();
        System.out.print("Enter the end: ");
        int end = input.nextInt();
        System.out.println("The composite numbers between " + start + " and " + end + " are:");
        for (int i = start; i <= end; i++) {
            if (isComposite(i)) {
                System.out.print(i + " ");
            }
        }
    }

    private static boolean isComposite(int n) {
        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                return true;
            }
        }
        return false;
    }
}
