//check given numer is prime or composite
import java.util.*;
class Program3 {
    public static void main(String[] args) {
       Scanner sc = new Scanner(System.in);
       System.out.println("enter the number:");
       int n = sc.nextInt();
       boolean isPrime = true;

       if(n <= 1) {
          isPrime = false;
       } else {
          for(int i = 2; i * i <= n;i++) {
	      if(n % i == 0) {
	       isPrime = false;
	       break;
	      }
	  }
       }

       if(isPrime) {
          System.out.println(n + "is prime number.");
       } else {
         System.out.println(n + "is composite number.");
       }
    }   
}
