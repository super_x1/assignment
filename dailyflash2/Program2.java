/*
 1 
 2 4
 3 6 9
 4 8 12 16
 */
class Program2 {
   public static void main(String[] args) {
   int n = 4;
   int count = 1;
   for(int i = 1;i <= n;i++) {
       for(int j = 1;j <= i;j++) {
         System.out.print(count + " ");
	 count += i;
       }
       System.out.println();
       count = i + 1;
   }
   }
}
