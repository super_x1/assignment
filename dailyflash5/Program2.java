/*
a 
A B 
a b c
A B C D

*/

class PatternPrint {
    public static void main(String[] args) {
        int n = 4; 

        for (int i = 1; i <= n; i++) {
            char letter = 'a';
            char capitalLetter = 'A';

            for (int j = 1; j <= i; j++) {
                if (i % 2 == 0) {
                    System.out.print(capitalLetter + " ");
                    capitalLetter++;
                } else {
                    System.out.print(letter + " ");
                    letter++;
                }
            }

            System.out.println();
        }
    }
}
