//print factorial of given range
import java.util.*;
class Program1 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println("Enter the start:");
    int start = sc.nextInt();
     System.out.println("Enter the end:");
    int end = sc.nextInt();

    int fact = 1;
      for(int i = start;i <= end;i++){
         fact = fact * i;
      }
      System.out.println(fact);
  }
}
