//
class Program4 {
    public static void main(String[] args) {
        int n = 4; // Change this value to adjust the number of rows and columns

        for (int i = 1; i <= n; i++) {
            int odd = i;
            int even = i + 1;

            for (int j = 1; j <= n; j++) {
                if (i % 2 == 0) {
                    System.out.print(even + " ");
                    even += 2;
                } else {
                    System.out.print(odd + " ");
                    odd += 2;
                }
            }

            System.out.println();
        }
    }
}
