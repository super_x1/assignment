/*
 A B C D
 D C B A
 A B C D
 D C B A
 */
class Program1 {
    public static void main(String[] args) {
       int n = 4; 
        char currentChar;

        for (int i = 1; i <= n; i++) {
            if (i % 2 == 1) {
                currentChar = 'A';
            } else {
                currentChar = 'D';
            }

            for (int j = 1; j <= n; j++) {
                System.out.print(currentChar + " ");
                if (currentChar == 'A') {
                    currentChar++;
                } else {
                    currentChar--;
                }
            }

            System.out.println(); 
        }
    }
}
