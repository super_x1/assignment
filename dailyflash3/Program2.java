/*
 1 
 2  1
 3  2  1
 4  3  2  1
 */
class Program2 {
   
    public static void main(String[] args) {
        int n = 4; // Number of rows
        for (int i = 1; i <= n; i++) {
            // Print spaces
            for (int j = 1; j <= n; j++) {
                System.out.print(" ");
            }

            // Print decreasing numbers
            for (int k = i; k >=1; k--) {
                System.out.print(k + "  ");
            }

            System.out.println(); // Move to the next line after each row
        }
  
     }
}
