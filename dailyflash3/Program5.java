//check whether string contains character other than letters
/*class CheckString {
    public static void main(String[] args) {
        String inputString = "YourStringHere"; // Replace with the string you want to check

        if (containsNonLetters(inputString)) {
            System.out.println("The string contains non-letter characters.");
        } else {
            System.out.println("The string consists of only letters.");
        }
    }

    public static boolean containsNonLetters(String str) {
        for (char c : str.toCharArray()) {
            if (!Character.isLetter(c)) {
                return true;
            }
        }
        return false;
    }
}*/
import java.util.Scanner;
class CheckString {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter a string: ");
        String input = sc.nextLine();
        boolean containsNonLetter = !input.matches("[a-zA-Z]+");
        if (containsNonLetter) {
            System.out.println("The string contains non-letter characters.");
        } else {
            System.out.println("The string contains only letters.");
        }
    }
}
