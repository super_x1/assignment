//print each reverse number in the given range
import java.util.*;
class Reverse {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the lower start of the range: ");
        int start = sc.nextInt();
        System.out.print("Enter the upper end of the range: ");
        int end = sc.nextInt();
        System.out.println("Reverse numbers in the given range are:");
        for (int i = start; i <= end; i++) {
            int reverse = 0;
            int number = i;
            while (number > 0) {
                int remainder = number % 10;
                reverse = reverse * 10 + remainder;
                number /= 10;
            }
            System.out.println(reverse);
        }
    }
}
