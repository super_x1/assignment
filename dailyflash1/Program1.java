/* print following pattern
 1  2  3  4
 2  3  4  5
 3  4  5  6
 4  5  6  7
*/
class Program1 {
    public static void main(String[] args) {
       int N = 4;
       for(int i = 1; i <= N ;i++) {
          for(int j = i; j < i + N ;j++) {
	    System.out.print(j + " ");
	    
	  }
            
	  System.out.println();
	 
       }
    }
}
