//count the size of given string
class Program5 {
     public static void main(String[] args) {
       String inputString = " Hello, world!";
       int count = 0;
       for(char character : inputString.toCharArray()) {
       count++;
       }
       System.out.println("The size of the string is " + count);
     }
} 
