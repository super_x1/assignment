//print odd numbers in the given range
import java.util.*;
class Program4 {
   public static void main(String[] args) {
     Scanner sc = new Scanner(System.in);
     System.out.println("Enter the start:");
     int start =sc.nextInt();
      System.out.println("Enter the end:");
      int end =sc.nextInt();
       for(int i = start;i <= end;i++) {
         if(i % 2 != 0){
	   System.out.println(i);
	 }
       }
   }
}
