//toggle the string lower case to upper case
class ToggleCase {
    public static void main(String[] args) {
        String input = "hello world";
        String output = "";
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (Character.isLowerCase(c)) {
                output += Character.toUpperCase(c);
            } else {
                output += Character.toLowerCase(c);
            }
        }
        System.out.println(output);
    }
}
