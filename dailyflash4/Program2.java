/*
 A 
 B A
 C B A
 D C B A
 */
class Program2 {
  public static void main(String[] args) {
   int n = 4;

        for (int i = 0; i < n; i++) {
            for (int j = i; j >= 0; j--) {
                System.out.print((char) ('A' + j) + " ");
            }
            System.out.println();
        }
    }
}
