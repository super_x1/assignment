/*
A B C D
# # # #
A B C D
# # # #
A B C D
*/
import java.util.*;
class Program1 {
    public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);
      System.out.println("Enter the number of rows");
      int row = sc.nextInt();
       System.out.println("Enter the number of col");
      int col = sc.nextInt();
      
  	    for (int i = 0; i < row; i++) {
              for (char ch = 'A'; ch <= 'D'; ch++) {
                 System.out.print(ch + " ");
              }
              System.out.println();

              for (int j = 0; j < col; j++) {
                 System.out.print("# ");
              }
            System.out.println();
        }
    }
}
